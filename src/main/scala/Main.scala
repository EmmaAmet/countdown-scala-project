import scala.annotation.{tailrec, unused}
import scala.collection.LinearSeq
import scala.io.{BufferedSource, Source, StdIn}
import scala.util.Random
import scala.util.control.NonLocalReturns.*

/**
 * Returns a a tuple consisting of two lists (stacks) of vowels and consonants with varying frequencies
 * The first element of the tuple is the vowel stack and second the consonant stack
 * @return
 */
def getLetterStacks: (LinearSeq[Char], LinearSeq[Char]) = {
  val vowels = Set('a', 'e', 'i', 'o', 'u')
  val letters: LinearSeq[Char] = LinearSeq.concat(for (x <- 'a' to 'z') yield x)
  val frequencies = LinearSeq(8, 2, 3, 4, 13, 2, 2, 6, 7, 1, 1, 4, 2, 7, 8, 2, 1, 6, 6, 9, 3, 1, 2, 1, 2, 1)
  val letterFrequencies: LinearSeq[(Char, Int)] = letters.zip(frequencies)

  val items = for ((k, v) <- letterFrequencies) yield k.toString * v
  val flatItems = Random.shuffle(items.flatten)
  flatItems.partition(c => vowels.contains(c))
}

/**
 * Receives a stack of vowels and consonants and recursively requests input from the user to obtain either a vowel or consonant character
 * Returns a list of 9 characters based on user choices. It ensures there are at least 4 consonants and 3 vowels
 *
 * @param vowels
 * @param consonants
 * @return
 */
def getUserLettersRec(vowels: LinearSeq[Char], consonants: LinearSeq[Char]): List[Char] = {
  println("Please select your letters: ")

  @tailrec
  def getUserLettersAcc(vowels: LinearSeq[Char], consonants: LinearSeq[Char], userVowels: List[Char], userConsonants: List[Char]): List[Char] = {
    println(s"Current choices ${userVowels.appendedAll(userConsonants)}")
    if (userVowels.length + userConsonants.length == 9) {
      userVowels.appendedAll(userConsonants)
    }
    else {
      println("Enter c for consonant v for vowel: \n")
      val input = StdIn.readLine()
      if (input == "c") {
        if (userConsonants.length <= 5) getUserLettersAcc(vowels, consonants.drop(1), userVowels, userConsonants :+ consonants.head) else getUserLettersAcc(vowels.drop(1), consonants, userVowels :+ vowels.head, userConsonants)
      } else {
        if (userVowels.length <= 4) getUserLettersAcc(vowels.drop(1), consonants, userVowels :+ vowels.head, userConsonants) else getUserLettersAcc(vowels, consonants.drop(1), userVowels, userConsonants :+ consonants.head)
      }
    }
  }

  getUserLettersAcc(vowels, consonants, List[Char](), List[Char]())
}

/**
 * Receives a list of letters and groups them to produce a map with individual characters as a key and the number of occurrences in the list as the value
 * @param letters
 * @return
 */
def getCharCount(letters: Seq[Char]): Map[Char, Int] = {
  letters.groupBy(identity).view.mapValues(_.size).toMap
}

/**
 * Helper method to remove an element at an index from a list
 * @param list
 * @param indexToRemove
 * @tparam A
 * @return
 */
def remove[A](list: Seq[A], indexToRemove: Int): Seq[A] = {
  list.slice(0, indexToRemove).appendedAll(list.slice(indexToRemove + 1, list.length))
}

/**
 * Ensures the count of characters in a given word are within the character counts for a larger list of words using recursion
 * @param word
 * @param corpus
 * @return
 */
def equalCharCountRec(word: String, corpus: List[Char]): Boolean = {
  @tailrec
  def equalCharCountRec(wordsLeft: Seq[Char], corpusLeft: Seq[Char]): Boolean = {
    if (wordsLeft.isEmpty) true
    else if (!corpusLeft.contains(wordsLeft.head)) false
    else {
      val indexToRemove = corpusLeft.indexOf(wordsLeft.head)
      equalCharCountRec(wordsLeft.drop(1), remove(corpusLeft, indexToRemove))
    }
  }

  equalCharCountRec(word, corpus)
}

/**
 * Compares the count of characters in a word to the count of characters in a larger list. Returns a boolean to indicate if all character frequencies in the word are within the list
 * @param word
 * @param corpus
 * @return
 */
@unused
def equalCharCount(word: String, corpus: List[Char]): Boolean = {
  val corpusCount = getCharCount(corpus)
  val wordCount = getCharCount(word)
  wordCount.keys.forall(c => {
    val wordValue = wordCount(c)
    val corpusValue = corpusCount.getOrElse(c, 0)
    if (corpusValue == 0) {
      false
    } else {
      wordValue <= corpusValue
    }
  })
}


/**
 * Checks that a given word is valid based on a list of characters
 * Ensures this by comparing presence and frequency of characters between word and list
 * Returns true to affirm this and false otherwise
 * @param word
 * @param corpus
 * @return
 */
def validWord(word: String, corpus: List[Char]): Boolean = {
  def containsCorrectLetters: Boolean = {
    word.forall(corpus.contains)
  }

  containsCorrectLetters && equalCharCountRec(word, corpus)
}


/**
 * Affirms that a given word can be found in given repository of dictionary values
 * @param word
 * @return Boolean
 */
def checkWordInDictionary(word: String, bufferedSource: BufferedSource): Boolean = returning {
  for (lines <- bufferedSource.getLines()) {
    if (lines.contains(word)) throwReturn(true)
  }
  bufferedSource.close()
  false
}

/**
 * Ascribes points to a word based on the given length. If word length is 9 ascribes 18 directly
 * @param word
 * @return
 */
def calculateWordPoints(word: String): Int = {
  if (word.length == 9) 18 else word.length
}

/**
 * Checks a given source of dictionary words for the longest words that are made up of letters from a sampleList pf characters
 * Returns the top two words that exist in that regard
 * @param sampleList
 * @return
 */
def getHighestScoringWords(sampleList: List[Char], bufferedSource: BufferedSource): (String, String) = {
  val lines = bufferedSource.getLines()

  @tailrec
  def getHighestScoringAcc(lines: Iterator[String], top2: (String, String)): (String, String) = {
    if (!lines.hasNext) top2
    else {
      val word = lines.next()
      val wordValid = validWord(word, sampleList)
      if (wordValid) {
        if (word.length > top2._1.length) {
          getHighestScoringAcc(lines, (word, top2._1))
        } else if (word.length > top2._2.length) {
          getHighestScoringAcc(lines, (top2._1, word))
        } else getHighestScoringAcc(lines, top2)
      } else getHighestScoringAcc(lines, top2)
    }
  }

  val top2 = getHighestScoringAcc(lines, ("", ""))
  bufferedSource.close()
  top2
}

def getPlayerInput(playerNum: Int): String = {
  println(s"Player $playerNum Please create a word from the above characters only: ")
  val input = StdIn.readLine()
  input
}

/**
 * Recursively requests and validates user input for a word. Ensures word is valid based on its presence and passing validator function provided
 * @param inputValidator
 * @return
 */
def getPlayerInputs(inputValidator: String => Boolean): (String, String) = {
  val validationMessage = "Invalid input for player. Please ensure your response is not empty and only available letters are used"

  @tailrec
  def getInputsAcc(playerInputs: (String, String)): (String, String) = {
    if (playerInputs._1.nonEmpty && playerInputs._2.nonEmpty) playerInputs
    else if (playerInputs._1.isEmpty) {
      val input = getPlayerInput(1)
      if (input.nonEmpty && inputValidator(input)) getInputsAcc((input, playerInputs._2))
      else {
        println(validationMessage)
        getInputsAcc(playerInputs)
      }
    } else {
      val input = getPlayerInput(2)
      if (input.nonEmpty && inputValidator(input)) getInputsAcc((playerInputs._1, input)) else {
        println(validationMessage)
        getInputsAcc(playerInputs)
      }
    }
  }

  getInputsAcc(("", ""))
}

/**
 * Checks length of two inputs to determine if winners are two or one
 * @param playerInputs
 * @return
 */
def determineWinners(playerInputs: (String, String)): (String, Option[String]) = {
  if (playerInputs._1.length == playerInputs._2.length) (playerInputs._1, Option(playerInputs._2))
  else if (playerInputs._1.length > playerInputs._2.length) (playerInputs._1, None)
  else (playerInputs._2, None)
}

@main def run(): Unit = {
  val (vowelStack, consonantStack) = getLetterStacks
  val userLetters = getUserLettersRec(vowelStack, consonantStack)
  println(s"These are the words to form from: $userLetters")

  val playerInputs = getPlayerInputs(word => {
    validWord(word, userLetters) && checkWordInDictionary(word, Source.fromFile("C:\\Users\\joseph.pobee\\IdeaProjects\\countdown\\src\\valid_words.txt"))
  })
  val winners = determineWinners(playerInputs)
  if (winners._2.isDefined) {
    //There are two winners
    println(s"Player 1 word: ${winners._1}, player 1 score: ${calculateWordPoints(winners._1)}")
    println(s"Player 2 word: ${winners._2.get}, player 2 score: ${calculateWordPoints(winners._2.get)}")
  } else {
    //There is one winner
    println(s"Winning word: ${winners._1} score: ${calculateWordPoints(winners._1)}")
  }

  val highestScoringWords = getHighestScoringWords(userLetters, Source.fromFile("C:\\Users\\joseph.pobee\\IdeaProjects\\countdown\\src\\valid_words.txt") )
  println(s"The highest scoring words are \n1. ${highestScoringWords._1} \n2. ${highestScoringWords._2}")


}